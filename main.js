// The ipcMain and ipcRenderer modules allow communication between the mai
// process and the renderer processes.
//
// For more info, see:
// https://electronjs.org/docs/api/ipc-main
// https://electronjs.org/docs/api/ipc-renderer

const { app, BrowserWindow, ipcMain } = require('electron');

if (require('electron-squirrel-startup')) app.quit();

const path = require('path');


app.whenReady().then(() => {
  const mainWindow = new BrowserWindow({
    height: 600,
    width: 800,
    webPreferences: {
      nodeIntegration: false, // default in Electron >= 5
      contextIsolation: true, // default in Electron >= 12
      sandbox: false,
      preload: path.join(__dirname, 'preload.js')
    }
  })
  // mainWindow.webContents.openDevTools();

  mainWindow.loadFile('index.html')
})
