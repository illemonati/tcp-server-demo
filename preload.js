const { ipcRenderer } = require('electron');
const fs = require('fs');
const net = require('net');

var os = require('os');

const networkInterfaces = os.networkInterfaces();

console.log(networkInterfaces);

const server = net.createServer();

const addMessage = (m) => {
  document.body.querySelectorAll('.messages').forEach(d => {
    if (m instanceof HTMLElement) {
      d.appendChild(m);
      return;
    }
    const p = document.createElement('p');
    p.innerText = m;
    d.appendChild(p);
  })
}

const scrollToBottom = () => {
  document.body.querySelectorAll('.messages').forEach(d => {
    d.scrollTop = d.scrollHeight;
  })
}

const broadcastToAll = (message, conns, exceptList) => {
  Object.entries(conns).forEach(([connStr, conn]) => {
    if (!exceptList?.includes(connStr)) {
      conn.write(message);
    }
  })
}

const connToString = (conn) => `${conn.remoteAddress}:${conn.remotePort}`;

const conns = {};
let currentInput = '';


const handleConnection = (conn) => {
  console.log(conn);
  const connStr = connToString(conn);
  conns[connStr] = conn;

  addMessage(`Connection from ${connStr}`);
  conn.on('data', (d) => {
    const message = `Message from ${connStr} : ${d}`;
    addMessage(message);
    scrollToBottom();
    broadcastToAll(message, conns, [connStr]);
  })
  conn.on('end', () => {
    addMessage(`End for ${connStr}`);
    delete conns[connStr];
  })
  conn.on('error', (e) => {
    addMessage(`Error for ${connStr} : ${e.message}`);
    delete conns[connStr];
  })
}

const changeAllInputs = () => {
  document.querySelectorAll('.message-input').forEach(i => {
    i.value = currentInput;
  })
}

const sendMessage = () => {
  console.log(currentInput);
  const message = `Message from server : ${currentInput}\n`;
  broadcastToAll(message, conns);
  addMessage(message);
  currentInput = '';
  changeAllInputs();
  scrollToBottom();
}

const bindInput = () => {
  console.log(1);
  document.querySelectorAll('.message-input').forEach(i => {
    console.log(i);
    i.addEventListener('input', e => {
      currentInput = e.target.value;
      changeAllInputs();
    })
    i.addEventListener('keypress', e => {
      if (e.key === "Enter") {
        sendMessage();
      }
    })
  })
  document.querySelectorAll('.message-send').forEach(b => {
    console.log(b);
    b.addEventListener('click', e => {
      sendMessage();
    })
  })
}

server.on('connection', handleConnection);

window.addEventListener('load', () => {
  const port = 12322;
  bindInput();
  server.listen(port, () => {
    const listenMessage = document.createElement('div');
    const listenOnH = document.createElement('h3');
    listenOnH.innerText = `Server listening on`;
    listenMessage.appendChild(listenOnH);
    Object.entries(networkInterfaces).forEach(([name, interface]) => {
      const block = document.createElement('div');
      const nameLabel = document.createElement('h5');
      nameLabel.innerText = name;
      block.appendChild(nameLabel);
      interface.forEach(a => {
        const addressLabel = document.createElement('p');
        addressLabel.innerText = a.address;
        block.appendChild(addressLabel);
      });
      block.style.marginBottom = '0.5rem';
      listenMessage.appendChild(block);
    })
    const portP = document.createElement('p');
    portP.innerText = `port ${port}`;
    listenMessage.appendChild(portP);
    addMessage(listenMessage);
    // scrollToBottom();
  });
})







